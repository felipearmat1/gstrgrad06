# Projeto da matéria SSC0965 - Streaming de Dados, Microsserviços e Containers (2022)

## Membros
- Felipe Araújo Matos NUSP: 5968691

## Aplicação
A aplicação deste projeto utilizará JavaScript (Node.js) com o framework React.js para o front-end. Essas tecnologias possuem as principais vantagens como:
- Linguagem amplamente utilizada e facilidade de desenvolvimento de projetos tanto para front-end como para back-end;
- Bibliotecas gratuitas e com código aberto;
- [Linguagem 'verde' e com bom desempenho no geral](https://medium.com/codex/what-are-the-greenest-programming-languages-e738774b1957);
- Comunidade ativa e vários fórum de dúvidas disponíveis;
Já como desvantagens temos:
- Linguagem interpretada, o que pode levar a erros inesperados durante a execução;
- Dinamicamente tipada, levando à menor garantia de compatibilidade entre as mensagens das funções/métodos;
- No desenvolvimento de front-end pode levar à problemas de comportamento entre os vários dispositivos e navegadores;
- Não oferece concorrência completa podendo levar à sub-utilização de infra-estrutura;
- Multi-paradigma, podendo levar à inconformidade se um padrão não for estabelecido no começo do projeto;

## Broker
Utilizaremos Kafka como broker para os eventos e mqtt. Essa tecnologia apresenta as seguintes vantagens:
- Sistema extremamente resiliente e escalável;
- Extensa opção de configurações e adaptadores;
- Utiliza conceitos de desenvolvimento que eliminam o trabalho fora do domínio da aplicação;
- Documentação e exemplos de arquivos Docker disponibilizados pela própria equipe de desenvolvimento;
- Integração direta com kubernets;
Já como desvantagens temos:
- Ferramenta complexa e com alta curva de aprendizado;
- Utiliza linguagem Java e adaptadores devem ser desenvolvidos na mesma linguagem;
- Pouco divulgada entre algumas linguagens e, consequentemente, com poucas bibliotecas para conectores;

## Armazenamento
Utilizaremos PostGres para a aplicação desejada. Essa tecnologia apresenta as seguintes vantagens:
- Banco relacional amplamente utilizado e robusto;
- Opções de configurações extensa e com muitas possibilidades de otimização para desempenho;
- Suporte a tipos customizados e com suporte nativo à JSON e XML;
- Muitas bibliotecas disponíveis em várias linguagens;
Já como desvantagens temos:
- Desempenho limitado ao desempenho de bancos relacionais;
- Não possibilidade de clusterização sem apoio de outras aplicações;
- Desempenho pode ser muito pobre se mal projetado e/ou configurado;

## Micro-serviço
Para micro-serviço utilizaremos javascript (Node.js) que deverão escrever ou consumir mensagens de nosso broker.
As vantagens e desvantagens são as mesmas do item [Aplicação](#aplicação), descartando os casos específicos de front-end.
