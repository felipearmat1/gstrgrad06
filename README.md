# GRUPO 13

## Como rodar o projeto

### Requisitos

O projeto tem os seguintes requisitos:

- docker
- docker-compose

### Executando o projeto

Para executar o projeto é necessário navegar até a root do mesmo e executar o comando:
```docker-compose -f docker-compose.yaml up```

## Introdução ao problema resolvido no projeto

### Discussão sobre a solução

O maior problema encontrado pelo grupo foi a capacidade de controlar os SONOFFs  sem a opção DIY (Do It Yourself) remotamente. Isso porque os modelos de dispositivo SONOFF que utilizamos só são controláveis através de um [servidor websocket que deveria ser configurado para operar em seu protocolo de comunicação](https://blog.ipsumdomus.com/sonoff-switch-complete-hack-without-firmware-upgrade-1b2d6632c01) e [utilizar um certificado específico que não está mais disponível desde a atualização de seu firmware para a versão v3](https://github.com/mdopp/simple-sonoff-server/issues/28). Para conseguir contornar esse problema utilizamos o sistema HomeAssistant que permite, dentre outras várias funcionalidades, que efetuemos a comunicação, tanto através de rede local, quanto através de aplicativos terceiros, dos SONOFFs em questão. Porém, ainda assim, para podermos utilizar o HomeAssistant sem ter que utilizar um sistema dedicado para o mesmo, fizemos a instalação do [plugin HACS](https://hacs.xyz/), que permite que utilizemos código produzido pela comunidade em qualquer instância do HomeAssistant, mesmo não utilizando um sistema dedicado ou o próprio sistema operacional do HomeAssistant. Com isso, utilizamos o plugin [SONOFFLan](https://github.com/AlexxIT/SonoffLAN) que pode ser instalado via HACS, e [configuramos os SONOFFs conforme sua documentação](https://github.com/AlexxIT/SonoffLAN#configuration-yaml).\
Um detalhe sobre o plugin SONOFFLan
A partir do momento que obtivemos os SONOFFs configurados se tornou fácil acionar ou coletar informações dos status dos SONOFFs, pois poderíamos utilizar a [API rest do HomeAssistant](https://developers.home-assistant.io/docs/api/rest/) para recursar dados e executar comandos nos SONOFFs. Ainda na etapa de configuração da API do HomeAssistant, tivemos que configurar um [token de utilização](https://developers.home-assistant.io/docs/auth_api/#long-lived-access-token) para que as chamadas fossem aceitas pela API do HomeAssistant.\
Para que todo esse sistema funcionasse de maneira prática configuramos o homeassistant localmente e exportamos suas configurações para o container docker, fazendo com que qualquer um com a imagem pudesse rodar o projeto de maneira simples e sem mais complicações. Também adicionamos essas configurações no repositório para que bastasse clonar e executar o comando do docker-compose.\
Dessa maneira, terminamos com uma solução que o usuário final tem apenas que executar os containers e adicionar os dispositivos que deseja no homeassistant, tendo toda a parte de comunicação e controle abstraída.\
A persistência dos dados é feita utilizando o banco de dados Postgres. Dessa maneira, conseguimos cadastrar e acompanhar os dispositivos controlados por nosso sistema.\
Transmissão de mensagens entre os serviços do backend é feita utilizando o HiveMQ, que utiliza o protocolo MQTT para fazer o envio das mesma, sendo uma maneira rápida e confiável de transmissão de informações. A arquitetura funciona utilizando a ideia de pub/sub, onde o backend realiza o envio das mensagens para o serviço responsável por realizar o toggle nos dispositivos desejados e outro tópico é utilizado para receber informações sobre o status dos dispositivos.\
A arquitetura geral está como mostrada na seguinte figura:\
![Alt text](images/arq_geral.jpg?raw=true "Arquitetura Geral")

## Back-end

### Função no projeto

Para que através de uma interface fosse possível controlar e utilizar a API do HomeAssistant, foi preciso criar uma aplicação para intermediar essas operações. Desse ponto de vista, o backend é responsável então pela comunicação com a base de dados, onde são cadastrados e armazenado dados sobre os dispositivos, comunicação com o HomeAssistant, no qual é responsável por se comunicar com a API e gerenciar as credenciais necessárias para acessa-la e, por fim, se comunicar com o broker. A comunicação com o broker é efetuada tanto para receber mensagens de status dos dispositivos quanto para enviar mensagens de acionamento para eles.\
É possível então entender o backend desenvolvido como dois serviços, um que expõem os endpoints permitindo que o frontend interaja com ele, registra e consulta registros no banco de dados e envia mensagens acionando os dispositivos. O outro seria o responsável pela comunicação com o HomeAssistant. Esse então roda um cronjob coletando o status dos dispositivos cadastrados e enviando esses status para o broker para que o outro registre no banco de dados, e também por receber as mensagens de acionamento enviadas no broker e realizando requisições para que o HomeAssistant execute essa ordem.\
A aplicação foi desenvolvida utilizando Python com FastAPI, SqlModels e paho/mqtt.\
A interação com o backend só pode ser efetuada através de seus endpoints. Para permitir que aplicações de outros domínios interajam com o backend foram aplicadas regras de permissão utilizamos o CORS para travar requisições para endpoints que não são GET. Então é preciso estar na mesma rede do backend para que possa utilizar todos os endpoints.\

### Tópicos

Quando executada, a aplicação tenta se conectar ao hivemq e se inscreve em dois tópicos: device/toggle e device/status/update e é através deles que os dois serviços interagem entre si.\

#### device/toggle

Ele é utilizado para realizar as alterações de estados dos dispositivos. Isto é possível de ser feito pelo id de um dispositivo ou por um grupo, alterando o estado de todos os dispositivos do grupo.\

#### device/status/update

Este é utilizado para atualizar o status do dispositivos cadastrados no sistema. É criado uma thread a parte, que é responsável por disparar um job que, a cada 5 segundos, obtém as informações sobre os status dos dispositivos e atualiza os mesmos na base de dados.

### Endpoint disponíveis

A aplicação está exposta na porta 8266, que é mapeada para a porta 8080 pelo docker.

#### GET ```/devices/group/{{group_name}}```

Este endpoint retorna todos os dispositivos presentes em um determinado grupo

#### GET ```/devices```

Este endpoint retorna todos os dispositivos presentes no sistema

#### POST ```/devices/group/toggle/{group_name}/{state}```

Este endpoint altera o status por grupo

#### POST ```/device/{{device_id}}/toggle```

Este endpoint altera o status por um device_id

#### POST ```/device```

Cria um novo dispositivo

## Front-end

Nossa app responsável pelo front-end foi escrita usando React. Ela é responsável por permitir a interação com o back-end através de sua interface. Também é responsável por realizar chamadas para busca de status dos dispositivos. Assim, o usuário consegue ter o controle dos dispositivos individualmente ou pelo grupo que o dispositivos se encontram.\
Abaixo segue uma imagem da aplicação listando os status dos dispositivos cadastrados:\
![Alt text](images/front-end-example.jpg?raw=true "front")

## Armazenamento

Escolhemos fazer o armazenamento utilizando o banco de dados Postgres. Alguns fatores nos levaram a essa escolha: é uma tecnologia opensource e resiliente, além do fato de que os membros do grupo estão mais familiarizados com ele.
As principais vantagens que encontramos com o uso do Postgres foram: a possibilidade de escalabilidade vertical (adicionando mais recursos computacionais para o banco) do Postgres, suporte para diversos tipos de dados personalizados, como XML e JSON, o fato de ser uma tecnologia de código aberto, com integração com várias tecnologias e bibliotecas para comunicação com o banco nas principais linguagens utilizadas atualmente e a disponibilidade de imagem no Dockerhub.\
A principal desvantagem comparado ao modelo não relacional é que ele tem um modelo de dados mais rígido e o que torna mudanças muito mais dificies e complexas.\
As tabelas são criadas a partir do arquivo ```schema.sql``` o qual é adicionado são elas as seguintes:\
![Alt text](images/tables.jpg?raw=true "Tables")

## HomeAssistant

Como já citado anteriormente, a escolha do sistema HomeAssistant se deu pela necessidade de comunicação com os SONOFFs até então restrita tanto pela falta da opção DIY, por não querermos utilizar um firmware não original e também pela atualização do certificado de comunicação a partir do firmware v3.\
O HomeAssistant foi modificado com o plugin HACS para poder utilizar a biblioteca SONOFFLan, com a instalação dessa biblioteca se tornou possível instalar a biblioteca [HASS-sonoff-ewelink](https://github.com/peterbuga/HASS-sonoff-ewelink) e com ela pudemos configurar mesmo o login via ewelink, quanto os SONOFFs individualmente na aplicação.\
Primeiramente é necessário configurar o plugin SONOFFLan no modo 'auto' (que permite ambas conexões local ou através do aplicativo eWeLink)
Se for efetuada a configuração através da aplicação eWeLink, seja via interface do próprio HomeAssistant ou através do [YAML de configuração do HomeAssistant](https://github.com/peterbuga/HASS-sonoff-ewelink#setup), os sonoffs serão automaticamente adicionados ao HomeAssistant sem mais complicações. Caso seja escolhido por fazer a configuração como servidor local, é necessário [conectar com o SONOFF diretamente para obter o devicekey](https://github.com/AlexxIT/SonoffLAN#getting-devicekey-manually), ou conectar pelo menos uma vez via aplicação do eWeLink para que o servidor do HomeAssistan consiga se comunicar com os dispositivos.\
Uma vez configurados os SONOFFs, é possível acessar a interface do próprio HomeAssistant para controlá-los.
