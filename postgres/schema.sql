DROP TABLE IF EXISTS Devices CASCADE;
CREATE TABLE Devices(
    id SERIAL PRIMARY KEY,
    device_name VARCHAR(255) not null, 
    device_id TEXT not null,
    group_name TEXT,
    device_status TEXT not null
);

ALTER TABLE Devices ADD CONSTRAINT unique_device_id UNIQUE (device_id);

INSERT INTO Devices (device_name, device_id, group_name, device_status) VALUES ('sonoff_1', '10014345a5', 'SALA', 'UNDEF');
INSERT INTO Devices (device_name, device_id, group_name, device_status) VALUES ('sonoff_2', '1001432cd3', 'SALA', 'UNDEF');


DROP TABLE IF EXISTS HomeAssistantTokens CASCADE;
CREATE TABLE HomeAssistantTokens(
    id INTEGER PRIMARY KEY,
    active BOOLEAN,
    token TEXT
);

INSERT INTO HomeAssistantTokens VALUES (1, true, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiI0NTcwOTFhNTE0MzQ0NDIwYjRiMTJhZTUxOWI5NGVkMCIsImlhdCI6MTY2OTUwNjYzNywiZXhwIjoxOTg0ODY2NjM3fQ.d6uPxiBzdPROHODP1sF6QrPu-n6U_fUIQ2bOP-unHAE');