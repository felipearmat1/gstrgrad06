from threading import Thread
import schedule
import requests
import time
from .client import client_connect
from app.logger import logger
from app.models.device_model import getDevicesQuery, updateDeviceStatus
from app.models.home_assistant_tokens_model import getHAToken
from app.database import engine
from sqlmodel import Session

# HiveMq connection
topic = "device/status/update"

#Function used when receiving a message from broker
def on_message(client, userdata, msg):
    logger.info('Received messsage from topic %s with body: %s.' % (msg.topic, msg.payload))
    with Session(engine) as session:
        logger.info("Trying to update device status")
        messageSplited = msg.payload.decode("utf-8").split(',')
        deviceId = messageSplited[0]
        deviceStatus = messageSplited[1]
        updateDeviceStatus(session, deviceId, deviceStatus)

def publishSensorDataMsg(client, message):
    try:
        # rc, mid = client.publish(topic, "10014345a5,ON")
        logger.info("Publishing on %s the following message: %s" % (topic, message))
        rc, mid = client.publish(topic, message)
        logger.info("Publishing result rc %s, mid %s" % (rc, mid))
    except Exception as e:
        logger.warn("Failed to publish msg to topic. Error %s" % str(e))

def getDeviceStatusFromHA(device_id, haToken):
    logger.info("Trying to perform request with the following params: %s and token %s." % (device_id, haToken))
    try:
        r = requests.get("http://homeassistant:8366/api/states/switch.sonoff_"+device_id, headers={"Content-Type":"application/json", "Authorization": "Bearer "+haToken})
        logger.info("GET from HA response: %s" % r.json())
        data = r.json()
        return data["state"]
    except Exception as e:
        logger.error("Failed to exec request. Error %s" % str(e))
        raise ValueError("Unable to perform request to HA")

def getAndPublishSensorDataMsg():
    try:
        with Session(engine) as session:
            # get homeassistant token
            haToken = getHAToken(session)
            logger.info("Retrived token %s" % haToken)

            # get devices to update status
            devices = getDevicesQuery(session)
            for device in devices:
                logger.info("Performing request to get status of device: %s" % str(device))
                deviceState = getDeviceStatusFromHA(device.device_id, haToken)
                publishSensorDataMsg(client, device.device_id + ',' + str(deviceState))
            logger.info("Successfully update all device status")
    except Exception as e:
        logger.error("Error while trying to get and publish")
        logger.error("Exception: %s" % str(e))

def runPending():
    while True:
        schedule.run_pending()
        time.sleep(1)

client = client_connect(topic)

client.on_message = on_message

schedule.every(1).seconds.do(getAndPublishSensorDataMsg)

thread = Thread(target=runPending)
thread.start()
