import requests
from app.logger import logger
from .client import client_connect
from app.models.home_assistant_tokens_model import getHAToken
from app.database import engine
from sqlmodel import Session
from app.logger import logger

# HiveMq connection
topic = "device/toggle"

#Function used when receiving a message from broker
def on_message(client, userdata, msg):
    logger.info('Received messsage from topic %s with body: %s.' % (msg.topic, msg.payload))
    try:
        with Session(engine) as session:
            # get homeassistant token
            haToken = getHAToken(session)
            logger.info("Retrived token %s" % haToken)
        toggleDevice(msg.payload.decode("utf-8"), haToken)

    except Exception as e:
        logger.error("Failed to execute toggle operation. Exception: %s" % str(e))

def publishToggleMsg(deviceId):
    try:
        # rc, mid = client.publish(topic, "10014345a5")
        rc, mid = client.publish(topic, deviceId)
        logger.info("Publishing result rc %s, mid %s" % (rc, mid))
    except Exception as e:
        logger.warn("Failed to publish msg to topic. Error %s" % str(e))

def toggleDevice(deviceId, haToken):
    try:
        body = {
            "entity_id": "switch.sonoff_" + deviceId
        }
        r = requests.post(
            "http://homeassistant:8366/api/services/switch/toggle",
            headers={"Content-Type":"application/json", "Authorization": "Bearer "+haToken},
            json=body
        )
        data = r.json()
        logger.info("POST for HA response: %s" % str(data))
    except Exception as e:
        logger.error("Failed to perform request to Home Assistant. Exception %s" % str(e))

client = client_connect(topic)

client.on_message = on_message
