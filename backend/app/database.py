import sys
from sqlmodel import create_engine
from app.config import config
from app.logger import logger

# Database connection
logger.info("Using sqlalchemy")
try:
    engine = create_engine(config.get('database_uri'), echo=True, future=True)
except:
    logger.error("Failed to connect to Data Base")
    sys.exit(1)
