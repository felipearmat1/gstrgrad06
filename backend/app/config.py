import os

config = {}

config['hivemq_port'] = int(os.getenv('HIVEMQ_PORT', '8466'))
config['hivemq_host'] = os.getenv('HIVEMQ_HOST', 'host.docker.internal')

config['database_uri'] = os.getenv('DATABASE_URI', 'postgresql://postgres:changeme@postgres:5432/postgres')
