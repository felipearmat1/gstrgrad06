from http.client import HTTPException
from sqlmodel import Field, SQLModel, select
from typing import Optional
from app.logger import logger

# Base class for inserting devices
class Devices(SQLModel, table=True):
    __tablename__ = "devices"

    id: Optional[int] = Field(default=None, primary_key=True)
    device_name: str = Field(unique=True)
    device_id: str = Field(unique=True)
    group_name: str
    device_status: Optional[str] = Field(default='UNDEF')

def getDevicesQuery(session):
    logger.info("Accessing database for Devices")
    values = session.query(Devices).all()
    logger.info("Success")
    return values

def getDeviceById(session, id):
    logger.info("Trying to retrieve device with id %s" % id)
    statement = select(Devices).where(Devices.id == id)
    device = session.exec(statement).first()
    return device

def createDeviceQuery(session, device: Devices):
    try:
        session.add(device)
    except Exception as e:
        logger.warn("Failed to create device. Error: %s" % str(e))
        raise ValueError("Device bad request")
    logger.info("Success")
    session.commit()
    return "Success"

def deleteDeviceQuery(session, id):
    logger.info("Trying to retrieve device with id %s" % id)
    device = session.get(Devices, id)
    if not device:
        raise HTTPException(status_code=404, detail="device not found")
    session.delete(device)
    session.commit()
    return {"ok": True}

def getDevicesByGroup(session, group_name):
    try:
        statement = select(Devices).where(Devices.group_name == group_name)
        return session.exec(statement)
    except Exception as e:
        logger.error("Failed to get devices by group")
        raise ValueError("Error while retrieving devices by group")

def updateDeviceStatus(session, deviceId, newStatus):
    statement = select(Devices).where(Devices.device_id == deviceId)
    device = session.exec(statement).first()
    if device is not None:
        logger.info("Found device: %s" % str(device))
        device.device_status = newStatus
        session.add(device)
        session.commit()
    else:
        logger.warn("Did not found any device with device_id %s" % deviceId)
        raise ValueError("Failed to retrive device with device_id: %s" % deviceId)
