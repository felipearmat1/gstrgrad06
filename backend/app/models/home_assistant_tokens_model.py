from sqlmodel import Field, SQLModel, select
from typing import Optional
from app.logger import logger

# Base class for inserting devices
class HATokens(SQLModel, table=True):
    __tablename__ = "homeassistanttokens"

    id: Optional[int] = Field(default=None, primary_key=True)
    active: bool
    token: str

def getHAToken(session):
    logger.info("Getting HA token from DB")
    statement = select(HATokens).where(HATokens.active == True)
    value = session.exec(statement).all()
    if value is not None:
        haTokens = value[0]
        logger.info("Founded token: %s ." % haTokens)
        return haTokens.token
    else:
        logger.warn("Did not found any active tokens at DB")
        raise ValueError("Could not retrive any active token from DB")
