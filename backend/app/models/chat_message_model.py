from datetime import datetime
from sqlmodel import Field, Column, DateTime, SQLModel, select
from typing import Optional
from app.logger import logger

# Base class for inserting chat messages
class ChatMessage(SQLModel, table=True):
    __tablename__ = "chat_messages"

    id: Optional[int] = Field(default=None, primary_key=True)
    user: str = Field(default='System')
    timestamp: datetime = Field(sa_column=Column(DateTime(timezone=True), nullable=False))
    chat_name: str = Field(default='System')
    message: str

def getChatMessagesQuery(session, chat_name):
    logger.info("Accessing database for chat messages")
    values = session.query(ChatMessage
                          ).where(ChatMessage.chat_name == chat_name
                          ).where(ChatMessage.chat_name == 'System'
                          ).all()
    logger.info("Success")
    return values

def createChatMessageQuery(session, chat_message: ChatMessage):
    try:
        session.add(chat_message)
    except Exception as e:
        logger.warn("Failed to create chatMessage. Error: %s" % str(e))
        raise ValueError("Device bad request")
    logger.info("Success")
    session.commit()
    return "Success"
