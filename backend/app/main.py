from fastapi import FastAPI, Response
from datetime import datetime
from fastapi.middleware.cors import CORSMiddleware
from sqlmodel import Session
from pydantic import BaseModel
from app.models.device_model import Devices, getDevicesQuery, createDeviceQuery, getDevicesByGroup, getDeviceById
from app.models.chat_message_model import getChatMessagesQuery, ChatMessage, createChatMessageQuery
from app.database import engine
from app.broker.broker_toggle_operation import publishToggleMsg
from app.logger import logger

# FastAPI config
app = FastAPI()

origins = [
    "http://localhost",
    "http://localhost:8080",
    "http://localhost:8166",
    "http://localhost:8266",
    "http://frontend:8166",
    "http://host.docker.internal:8166",
    "http://host.docker.internal:8266"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"]
)

class CreateDevice(BaseModel):
    device_name: str
    device_id: str
    group_name: str

class CreateChatMessage(BaseModel):
    user: str
    message: str
    chat_name: str

# Endpoints
# Device
@app.get("/devices")
def getDevices():
    with Session(engine, future=True) as session:
        return getDevicesQuery(session)

@app.post("/device")
def createDevice(device: CreateDevice):
    with Session(engine, future=True) as session:
        logger.info("Received the following body: %s" % device)
        deviceModel = Devices(device.device_name, device.device_id, device.group_name)
        return createDeviceQuery(session, deviceModel)

@app.get("/devices/group/{group}")
def getDevicesByGroupController(group: str):
    with Session(engine, future=True) as session:
        logger.info("Getting devices from group: %s" % group)
        return getDevicesByGroup(session, group)


# Device toggle
@app.post("/device/{id}/toggle")
def toggleDeviceById(id, response: Response):
    with Session(engine) as session:
        logger.info("getting HA device id of device")
        device = getDeviceById(session, id)
        publishToggleMsg(device.device_id)

@app.post("/devices/group/toggle/{group_name}/{state}")
def toggleDevicesByGroup(group_name, state, response: Response):
    with Session(engine) as session:
        logger.info("Toggling devices of group: %s" % group_name)
        devices = getDevicesByGroup(session, group_name)
        if devices is not None:
            for device in devices:
                if(device.device_status != state):
                    publishToggleMsg(device.device_id)

@app.get("/chat/{chat_name}")
def getChat(chat_name: str):
    with Session(engine) as session:
        logger.info("Getting chat messages from chat: %s" % chat_name)
        return getChatMessagesQuery(session, chat_name)

@app.post("/chat_message/")
def getChat(chat_message: CreateChatMessage):
    with Session(engine, future=True) as session:
        logger.info("Creating chat message for chat: %s" % CreateChatMessage.chat_name)
        if chat_message.chat_name == 'System':
            return 'Messages to System chat are not allowed'
        chatMessageModel = ChatMessage(
            chat_message.user,
            datetime.now,
            chat_message.chat_name,
            chat_message.message,
        )
        return createChatMessageQuery(session, chatMessageModel)


logger.info("App finished starting")
