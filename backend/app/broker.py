
import sys
import paho.mqtt.client as paho
from app.logger import logger
from app.config import config

# HiveMq connection
topic = "mockedtopic/tests/sonoff"
# Function used as callback when connection is established
def on_connect(client, userdata, flags, rc):
    if rc == 0:
        logger.info('CONNACK received with code %d. Everything is fine.' % (rc))
    else:
        logger.error("Failed to connect to broker. Userdata %s, flags %s, rc %s" %(str(userdata), str(flags), str(rc)))

#Function used when receiving a message from broker
def on_message(client, userdata, msg):
    logger.info('Received messsage from topic %s with body: %s.' % (msg.topic, msg.payload))

def on_publish(client, userdata, mid):
    logger.info("Publishing callback. Userdata %s, mid %s" % (str(userdata), str(mid)))

def on_subscribe(client, userdata, mid, granted_qos):
    logger.info("On subscribe result. Client %s, userdata %s, mid %s, granted_qos %s" % (str(client), str(userdata), str(mid), str(granted_qos)))

def on_log(client, userdata, level, buf):
    logger.info("buf: %s." % buf)

def publishTestMsg(client):
    try:
        rc, mid = client.publish(topic, "testing")
        logger.info("Publishing result rc %s, mid %s" % (rc, mid))
    except Exception as e:
        logger.warn("Failed to publish msg to topic. Error %s" % str(e))

try:
    client = paho.Client()
    client.on_connect = on_connect
    client.on_publish = on_publish
    client.on_log = on_log
    client.on_subscribe = on_subscribe
    client.on_message = on_message
    # client.username_pw_set('admin', 'hivemq')
    client.connect(config.get('hivemq_host'), config.get('hivemq_port'), 60)
    client.loop_start()
    logger.info("subscribing to topic %s" % topic)
    client.subscribe(topic)
except Exception as e:
    logger.error("Failed to connect on HiveMq. Error: %s" % str(e))
    sys.exit(1)
