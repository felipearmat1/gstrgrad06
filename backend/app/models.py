from sqlmodel import Field, SQLModel
from app.logger import logger

# Base class for inserting devices
class Devices(SQLModel, table=True):
    __tablename__ = "devices"

    id: int = Field(primary_key=True)
    device_name: str = Field(unique=True)
    device_ip: str = Field(unique=True)

def getDevicesQuery(session):
    logger.info("Accessing database for Devices")
    values = session.query(Devices).all()
    logger.info("Success")
    return values

def createDeviceQuery(session, device: Devices):
    try:
        session.add(device)
    except Exception as e:
        logger.warn("Failed to create device. Error: %s" % str(e))
        raise ValueError("Device bad request")
    logger.info("Success")
    session.commit()
    return "Success"
