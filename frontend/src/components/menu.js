import React from 'react'
import Container from 'react-bootstrap/Container'
import Nav from 'react-bootstrap/Nav'
import Navbar from 'react-bootstrap/Navbar'
import NavDropdown from 'react-bootstrap/NavDropdown'
import PropTypes from 'prop-types'
import { LinkContainer } from 'react-router-bootstrap'

const Menu = ({ items }) => {
  const renderItem = (item, key) => {
    if (item.children) {
      return (
        <NavDropdown
          title={item.title}
          key={key + '_dropdown'}
        >
          {
            item.children.map((child, idx) => {
              return (
                <LinkContainer
                  key={idx + '_container'}
                  to={`${item.path}/${child.path}`}
                >
                  <NavDropdown.Item
                    id={`${key}_${idx}`}
                    key={idx + '_item'}
                  >
                    {child.title}
                  </NavDropdown.Item>
                </LinkContainer>
              )
            })
          }
        </NavDropdown>
      )
    }
    return (
      <LinkContainer
        key={key + '_container'}
        to={item.path}
      >
        <Nav.Link key={key + '_item'}>
          {item.title}
        </Nav.Link>
      </LinkContainer>
    )
  }

  return (
    <Navbar
      bg="dark"
      variant="dark"
      expand="lg"
      sticky="top"
    >
      <Container>
        <Navbar.Brand href="/">GIOTGRAD13</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            { items.map((item, idx) => renderItem(item, idx)) }
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}

const itemDef = PropTypes.shape({
  title: PropTypes.string.isRequired,
  path: PropTypes.string.isRequired,
  children: PropTypes.arrayOf(PropTypes.shape({
    title: PropTypes.string.isRequired,
    path: PropTypes.string.isRequired
  }))
})

Menu.propTypes = {
  items: PropTypes.arrayOf(itemDef).isRequired
}

export default Menu
