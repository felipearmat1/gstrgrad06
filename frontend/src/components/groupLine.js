import React from 'react'
import PropTypes from 'prop-types'
import Form from 'react-bootstrap/Form'
import ItemTable from './itemTable'

const groupLine = ({groupItem, callBack, ...rest}) => {
  const transformGroup = {
    'status': ({value, item}) => {
      return (
        <Form.Check
          type="switch"
          id={item.name}
          checked={value}
          onChange={callBack(item.name)}
        />
      )
    }
  }

  return (
    <ItemTable
      variant="dark"
      items={[groupItem]}
      transform={transformGroup}
      order={['name', 'status']}
      headerLess
      striped
      bordered
      {...rest}
    />
  )
}

groupLine.propTypes = {
  groupItem: PropTypes.shape({
    name: PropTypes.string.isRequired,
    status: PropTypes.bool.isRequired
  }).isRequired,
  callBack: PropTypes.func.isRequired
}

export default groupLine
