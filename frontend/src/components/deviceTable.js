import React from 'react'
import Form from 'react-bootstrap/Form'
import ItemTable from '../components/itemTable'

const DeviceTable = ({deviceItems, callBack, ...rest}) => {
  const transform = {
    'device_status': ({value, item}) => {
      return (
        <Form.Check
          type="switch"
          id={item.id}
          checked={value}
          onChange={callBack(item.id)}
        />
      )
    }
  }

  return (
    <ItemTable
      variant="dark"
      items={deviceItems}
      transform={transform}
      order={['group_name', 'device_id', 'device_name']}
      striped
      bordered
      hover
      {...rest}
    />
  )
}

export default DeviceTable
