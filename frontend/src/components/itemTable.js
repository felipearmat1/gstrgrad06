import React from 'react'
import Table from 'react-bootstrap/Table'
import PropTypes from 'prop-types'

const ItemTable = ({
  items,
  transform = {},
  order = [],
  exclude = [],
  headerLess = false,
  ...args
}) => {
  const keys = (items) => {
    const keysVec = items.map(obj => Object.keys(obj))
    const filteredKeys = keysVec.filter(key => !exclude.includes(key))
    return [...new Set([].concat.apply(order, filteredKeys))]
  }

  const header = (keys) => {
    if (!headerLess) {
      return (
        <thead>
          <tr>
            {
              keys.map(key => {
                return (
                  <th key={key + '_header'}>
                    { key }
                  </th>
                )
              })
            }
          </tr>
        </thead>
      )
    }
  }

  const body = (keys, items) => {
    return items.map((item, idx) => {
      return (
        <tr key={idx}>
        {
          keys.map(key => {
            let value = item[key]
            return (
              <td key={`${idx}_${key}`}>
                { transform[key] ? transform[key]({value, item}) : value }
              </td>
            )
          })
        }
        </tr>
      )
    })
  }

  return (
    <Table { ...args }>
      { header(keys(items)) }
      <tbody>
        { body(keys(items), items) }
      </tbody>
    </Table>
  )
}

ItemTable.propTypes = {
  items: PropTypes.arrayOf(PropTypes.object).isRequired,
  transform: PropTypes.object,
  exclude: PropTypes.array,
  headerLess: PropTypes.bool,
  order: PropTypes.array
}

export default ItemTable
