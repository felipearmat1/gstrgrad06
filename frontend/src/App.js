import React from 'react'
import Router from './routes/router'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css'

const router = createBrowserRouter(Router);

function App() {
  return (
    <RouterProvider router={router} />
  )
}

export default App
