import React from 'react'
import PropTypes from 'prop-types'
import Container from 'react-bootstrap/Container'
import Menu from '../components/menu'
import Router from '../routes/router'

const singleColumn = ({ appHeader, appMain, appFooter }) => {
  function genContainer(child, id = null, defaultChild) {
    const el = child || defaultChild
    if(el) {
      return (
        <Container
          id={id}
          className={'p-0 m-0'}
          fluid
        >
          { el }
        </Container>
      )
    }
  }

  const defaultHeader = Menu({items: Router})

  return (
    <div>
      { genContainer(appHeader, 'app_header', defaultHeader) }
      <Container id="app_main">
        { appMain }
      </Container>
      { genContainer(appFooter, 'app_footer') }
    </div>
  )
}
export default singleColumn

singleColumn.propTypes = {
  appMain: PropTypes.node.isRequired
}
