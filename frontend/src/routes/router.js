import Main from '../views/main'
import Devices from '../views/devices'
import Groups from '../views/groups'
import { getDevices } from '../actions/getDevices'

const Router = [
  {
    path: '/',
    title: 'Home',
    index: true,
    element: <Main />
  },
  {
    path: '/devices',
    title: 'Dispositivos',
    children: [
      {
        path: 'create',
        title: 'Criar',
        element: <Main />
      },
      {
        path: 'list',
        title: 'Listar',
        loader: getDevices,
        element: <Devices />
      }
    ]
  },
  {
    path: '/environments',
    title: 'Ambientes',
    children: [
      {
        path: 'list',
        title: 'Listar',
        loader: getDevices,
        element: <Groups />
      }
    ]
  }
]

export default Router
