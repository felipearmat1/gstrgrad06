import axios from 'axios'

export async function getDevices() {
  let res = await axios.get(process.env.REACT_APP_BACKEND_ROUTE + '/devices')
  let devices = res.data
  return devices
}
