import axios from 'axios'

export async function toggleGroup(groupName, state) {
  let baseRoute = process.env.REACT_APP_BACKEND_ROUTE
  let stateName = state === true || 'on' ? 'on' : 'off'
  let route = baseRoute + `/devices/group/toggle/${groupName}/${stateName}`
  let res = await axios.post(route)
  return res.data
}
