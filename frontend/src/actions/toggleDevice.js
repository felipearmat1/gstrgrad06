import axios from 'axios'

export async function toggleDevice(deviceId) {
  let baseRoute = process.env.REACT_APP_BACKEND_ROUTE
  let route = baseRoute + `/device/${deviceId}/toggle`
  let res = await axios.post(route)
  return res.data
}
