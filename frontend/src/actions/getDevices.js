import axios from 'axios'

export async function getDevices() {
  let baseRoute = process.env.REACT_APP_BACKEND_ROUTE
  let res = await axios.get(baseRoute + '/devices')
  let devices = res.data
  return devices
}
