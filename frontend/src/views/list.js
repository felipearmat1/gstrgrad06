import React from 'react'
import SingleColumn from '../layouts/single_column'
import Menu from '../components/menu'
import ItemTable from '../components/itemTable'
import { useLoaderData } from "react-router-dom"
import Router from '../routes/router'

const List = () => {
  const data = useLoaderData()

  const list = (
    <div className="App">
      <header className="App-header" />
      <ItemTable
        variant="dark"
        items={data}
        order={['id', 'device_ip']}
        striped
        bordered
        hover
      />
    </div>
  )

  return (
    <SingleColumn
      appHeader={Menu({items: Router})}
      appMain={list}
    />
  )
}

export default List
