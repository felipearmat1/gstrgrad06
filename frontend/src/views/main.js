import React from 'react'
import logo from '../logo.svg'
import SingleColumn from '../layouts/single_column'
import Menu from '../components/menu'
import Router from '../routes/router'

const Main = () => {
  const main = (
    <div className="App">
      <header className="App-header">
        <img
          src={logo}
          className="App-logo"
          alt="logo"
        />
      </header>
    </div>
  )

  return (
    <SingleColumn
      appHeader={Menu({items: Router})}
      appMain={main}
    />
  )
}

export default Main
