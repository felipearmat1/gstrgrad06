import React, { useState } from 'react'
import SingleColumn from '../layouts/single_column'
import GroupLine from '../components/groupLine'
import DeviceTable from '../components/deviceTable'
import useLoop from '../hooks/useLoop'
import { useLoaderData } from "react-router-dom"
import { getDevices } from '../actions/getDevices'
import { toggleDevice } from '../actions/toggleDevice'
import { toggleGroup } from '../actions/toggleGroup'

const Groups = () => {
  const parseValues = source => {
    let response = {}
    source.map(item => {
      response[item.id] = ({
        ...item,
        ['device_status']: item.device_status === 'on'
      })
    })
    return response
  }

  const [items, setItems] = useState(parseValues(useLoaderData()))

  const loopCallBack = () => {
    getDevices().then(res => {
      setItems(parseValues(res))
    })
  }

  const { resetLoop } = useLoop(loopCallBack)

  const groups = () => {
    return Object
      .values(items)
      ?.map((device) => device.group_name)
      ?.filter((val, idx, arr) => arr.indexOf(val) === idx)
  }

  const filterByGroup = groupName => {
    return Object
      .values(items)
      ?.filter(item => item.group_name === groupName)
  }

  const groupStatus = groupName => {
    return filterByGroup(groupName).every(item => item.device_status === true)
  }

  const updateStatus = (deviceId, deviceStatus) => {
    let newObject = items[deviceId]
    newObject['device_status'] = deviceStatus
    setItems(prevStatus => ({
      ...prevStatus,
      [deviceId]: newObject
    }))
  }

  const updateStatusHandler = deviceId => {
    const updater = event => {
      toggleDevice(deviceId)
      resetLoop()
      updateStatus(deviceId, event.target.checked)
    }
    return updater
  }

  const updateGroupStatus = (groupName, status) => {
    let devices = filterByGroup(groupName)
    let newItems = {}
    devices.map(device => {
      device['device_status'] = status
      newItems[device.id] = device
    })
    setItems(prevStatus => ({
      ...prevStatus,
      ...newItems
    }))
  }

  const updateGroupsHandler = groupName => {
    const updater = event => {
      let newStatus = event?.target?.checked
      toggleGroup(groupName, newStatus)
      resetLoop()
      updateGroupStatus(groupName, newStatus)
    }
    return updater
  }

  const groupTable = groupName => {
    const groupItem = { 'name': groupName, 'status': groupStatus(groupName) }
    return (
      <div key={groupName + '_table'}>
        <p>{ JSON.stringify(groupItem) }</p>
        <GroupLine
          groupItem={groupItem}
          callBack={updateGroupsHandler}
        />
        <DeviceTable
          deviceItems={filterByGroup(groupName)}
          callBack={updateStatusHandler}
          exclude={['group_name']}
          headerLess
        />
      </div>
    )
  }

  const list = (
    <div className="App">
      <header className="App-header" />
      { groups().map(group => {
        return groupTable(group)
      }) }
    </div>
  )

  return (
    <SingleColumn appMain={list} />
  )
}

export default Groups
