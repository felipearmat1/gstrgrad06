import React, { useState } from 'react'
import SingleColumn from '../layouts/single_column'
import DeviceTable from '../components/deviceTable'
import useLoop from '../hooks/useLoop'
import { useLoaderData } from "react-router-dom"
import { getDevices } from '../actions/getDevices'
import { toggleDevice } from '../actions/toggleDevice'

const Devices = () => {
  const parseValues = source => {
    let response = {}
    source.map(item => {
      response[item.id] = ({
        ...item,
        ['device_status']: item.device_status === 'on'
      })
    })
    return response
  }

  const data = useLoaderData()

  const [items, setItems] = useState(parseValues(data))

  const loopCallBack = () => {
    getDevices().then(res => {
      setItems(parseValues(res))
    })
  }

  const { resetLoop } = useLoop(loopCallBack)

  const updateStatus = (deviceId, deviceStatus) => {
    let newObject = items[deviceId]
    newObject['device_status'] = deviceStatus
    setItems(prevStatus => ({
      ...prevStatus,
      [deviceId]: newObject
    }))
  }

  const updateStatusHandler = deviceId => {
    const updater = event => {
      toggleDevice(deviceId)
      resetLoop()
      updateStatus(deviceId, event.target.checked)
    }
    return updater
  }

  const list = (
    <div className="App">
      <header className="App-header" />
      <DeviceTable
        deviceItems={Object.values(items)}
        callBack={updateStatusHandler}
      />
    </div>
  )

  return (
    <SingleColumn appMain={list} />
  )
}

export default Devices
