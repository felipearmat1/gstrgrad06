import { useEffect, useRef } from 'react'

export default function useLoop(
  callBack,
  timeout = 2000,
  timer = null
){
  const defaultTimer = useRef(null)

  const localTimer = timer || defaultTimer

  const startLoop = (timer) => {
    timer.current = setInterval(() => {
      callBack()
    }, timeout)
  }

  const resetLoop = () => {
    clearInterval(localTimer.current)
    startLoop(localTimer)
  }

  useEffect(() => {
    // Start the loop on component mount
    startLoop(localTimer)
    // Clear the loop on component unmount
    return () => clearInterval(localTimer.current)
  }, [])

  return { resetLoop }
}
